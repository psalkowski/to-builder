(function () {
    'use strict';

    angular
        .module('toBuilder')
        .run(runBlock);

    /** @ngInject */
    function runBlock($log, Widget) {

        $log.debug('runBlock end');

        Widget.register('RowWidget');
        Widget.register('TextWidget');
    }

})();
