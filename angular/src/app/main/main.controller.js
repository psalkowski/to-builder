(function () {
    'use strict';

    angular
        .module('toBuilder')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($uibModal, $log) {
        var vm = this;
        var counter = 0;

        var widgets = [
            {
                label: 'Wiersz',
                type: 'row',
                controller: 'RowController',
                template: 'app/components/row/modal.html',
                columns: [{
                    size: 12,
                    items: []
                }],
                content: {
                    columns: '1/1'
                },
                exclude: true
            },
            {
                label: 'Tekst',
                type: 'text',
                controller: 'TextWidgetController',
                content: {},
                template: 'app/components/text/modal.html'
            }
        ];

        vm.items = {
            selected: null,
            rows: []
        };

        vm.itemsAllowedTypes = widgets.map(function(item) {
            if(!item.exclude) {
                return item.type;
            }
        }).filter(function(item) {
            if(item) {
                return item;
            }
        });

        vm.addItem = function (list) {
            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                resolve: {
                    widgets: function() {
                        return widgets;
                    }
                },
                size: 'full',
                templateUrl: 'app/components/widget/widget-list-modal.html',
                controllerAs: 'vm',
                bindToController: true,
                /** @ngInject */
                controller: function($uibModalInstance, widgets) {
                    var vm = this;
                    vm.widgets = widgets;
                    vm.widget = null;

                    vm.select = function(widget) {
                        vm.widget = widget;
                    };

                    vm.ok = function () {
                        $uibModalInstance.close(vm.widget);
                    };

                    vm.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };
                }
            });

            modalInstance.result.then(function (result) {
                result.id = ++counter;
                list.push(angular.copy(result));
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };

        vm.addRow = function () {
            vm.items.rows.push(angular.copy(getWidget('row')));
        };

        vm.removeItem = function (array, index) {
            array.splice(index, 1);
        };

        function getWidget(type) {
            var res = null;
            angular.forEach(widgets, function(widget) {
                if(widget.type === type) {
                    res = widget;
                    return false;
                }
            });

            return res;
        }

        vm.open = function (item) {
            if (!item.hasOwnProperty('type')) {
                return;
            }

            var widget = getWidget(item.type);

            if (!widget) {
                $log.debug('Undefined widget ', item.type);
                return;
            }

            var modalInstance = $uibModal.open({
                animation: true,
                backdrop: 'static',
                resolve: {
                    item: angular.copy(item)
                },
                size: 'full',
                templateUrl: widget.template,
                controller: widget.controller,
                controllerAs: 'vm',
                bindToController: true
            });

            modalInstance.result.then(function (result) {
                if(result) {
                    angular.extend(item, result);
                }
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };
    }
})();
