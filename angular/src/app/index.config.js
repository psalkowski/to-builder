(function() {
  'use strict';

  angular
    .module('toBuilder')
    .config(config);

  /** @ngInject */
  function config($logProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
  }

})();
