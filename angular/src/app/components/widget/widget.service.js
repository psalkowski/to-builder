(function () {
    'use strict';

    angular
        .module('toBuilder')
        .service('Widget', Widget);

    /** @ngInject */
    function Widget($injector, $log) {
        var widgets = {};
        var items = [];

        return {
            add: function(service) {
                var item = {
                    id: items.length,
                    model: service.newModel(),
                    service: service
                };

                items.push(item);

                return item;
            },
            edit: function() {

            },
            remove: function(service) {
                return items.splice(items.indexOf(service), 1);
            },
            register: function(widgetName) {
                if($injector.has(widgetName)) {
                    widgets[widgetName] = $injector.get(widgetName);
                } else {
                    $log.error('Widget ' + widgetName + ' does not exist.');
                }
            },
            getWidgets: function() {
                return widgets;
            },
            getItems: function() {
                return items;
            }
        };
    }
})();