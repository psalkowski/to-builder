(function () {
    'use strict';

    angular
        .module('toBuilder')
        .service('RowWidget', RowWidget);

    /** @ngInject */
    function RowWidget() {
        var template = 'app/components/row/form.html';
        var sidebar = 'app/components/row/sidebar.html';
        var name = 'Row';

        return {
            name: name,
            template: template,

            sidebar: {
                form: sidebar
            },

            newModel: function() {
                return {
                    columns: 1,
                    availableColumns: [1,2,3,4,6],
                    data: []
                }
            },
            change: function() {

            }
        };
    }
})();