(function () {
    'use strict';

    angular
        .module('toBuilder')
        .controller('RowController', RowController);

    /** @ngInject */
    function RowController($scope, $uibModalInstance, item) {
        var vm = this;

        if (!item.hasOwnProperty('content') || !angular.isObject(item.content)) {
            item.content = {};
        }

        vm.simpleColumns = {
            '1/1': '1',
            '1/2 1/2': '1/2 1/2',
            '1/3 1/3 1/3': '1/3 1/3 1/3',
            '1/2 1/4 1/4': '1/2 1/4 1/4'
        };

        vm.model = item.content;
        var columns = vm.model.columns;

        $scope.$watch(function() {
            return vm.model.simpleColumns;
        }, function() {
            vm.model.columns = vm.model.simpleColumns;
            if(columns) {
                vm.model.columns = columns;
                columns = null;
            }
        });

        vm.ok = function () {
            if(update()) {
                $uibModalInstance.close(item);
            }
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        function update() {
            var wantedColumns = vm.model.columns.split(' ');
            var columnCount = wantedColumns.length;
            var columns = item.columns;
            var i;

            if(columnCount > columns.length) {
                for(i = columns.length; i < columnCount; i++) {
                    columns.push({
                        size: null,
                        items: []
                    });
                }
            } else if(columnCount < columns.length) {
                var lastColumn = columns[columnCount-1];

                for(i = columns.length - 1; i >= columnCount; i--) {
                    lastColumn.items = lastColumn.items.concat(columns[i].items);
                    columns.splice(i, 1);
                }
            }

            var tmp = [];
            var total = 0;
            for(i = 0; i < columnCount; i++) {
                var math = wantedColumns[i].split('/');
                var res = math[0] / math[1]* 12;

                if(isInt(res)) {
                    tmp.push(math[0] / math[1]* 12);
                    total += tmp[i];
                }
            }

            if(tmp.length !== wantedColumns.length || total > 12) {
                return false;
            }

            for(i = 0; i < tmp.length; i++) {
                columns[i].size = tmp[i];
            }

            return true;
        }

        function isInt(n){
            return Number(n) === n && n % 1 === 0;
        }
    }
})();
