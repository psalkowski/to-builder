(function () {
    'use strict';

    angular
        .module('toBuilder')
        .directive('tbRow', tbRow);

    /** @ngInject */
    function tbRow() {
        var directive = {
            restrict: 'AE',
            templateUrl: 'app/components/row/row.html',
            scope: {
                columns: '=tbRow'
            },
            link: link,
            controller: controller,
            controllerAs: 'vm',
            bindToController: true
        };

        return directive;

        function link() {

        }

        /** @ngInject */
        function controller($scope) {
            var vm = this;
            vm.maxColumns = 12;

            $scope.$watch(function() {
                return vm.columns;
            }, function() {
                vm.columnClass = 'col-md-' + (vm.maxColumns / vm.columns);
            });
        }
    }
})();