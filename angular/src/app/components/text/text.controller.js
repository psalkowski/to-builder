(function () {
    'use strict';

    angular
        .module('toBuilder')
        .controller('TextWidgetController', TextWidgetController);

    /** @ngInject */
    function TextWidgetController($uibModalInstance, item) {
        var vm = this;

        if (!item.hasOwnProperty('content') || !angular.isObject(item.content)) {
            item.content = {};
        }

        vm.model = item.content;

        vm.ok = function () {
            $uibModalInstance.close(item);
        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    }
})();
