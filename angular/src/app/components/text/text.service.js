(function () {
    'use strict';

    angular
        .module('toBuilder')
        .service('TextWidget', TextWidget);

    /** @ngInject */
    function TextWidget() {
        var template = 'app/components/text/form.html';
        var sidebar = 'app/components/text/sidebar.html';
        var name = 'Text';

        return {
            name: name,
            template: template,
            sidebar: {
                form: sidebar
            },
            newModel: function() {
                return {
                    name: 'lalala',
                    title: 'lalalala'
                }
            }
        };
    }
})();