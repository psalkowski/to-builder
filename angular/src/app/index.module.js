(function() {
  'use strict';

  angular
    .module('toBuilder', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.bootstrap', 'angular-click-outside', 'dndLists']);

})();
