'use strict';

var path = require('path');
var gulp = require('gulp');
var conf = require('./conf');

var $ = require('gulp-load-plugins')();

var wiredep = require('wiredep').stream;
var _ = require('lodash');


gulp.task('dev:partials', function () {
    return gulp.src([
            path.join(conf.paths.src, '/app/**/*.html'),
            path.join(conf.paths.tmp, '/serve/app/**/*.html')
        ])
        .pipe($.minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe($.angularTemplatecache('app.tpl.js', {
            module: 'toBuilder',
            root: 'app'
        }))
        .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('dev:styles', function() {
    var sassOptions = {
        style: 'expanded'
    };

    var injectFiles = gulp.src([
        path.join(conf.paths.src, '/app/**/*.scss'),
        path.join('!' + conf.paths.src, '/app/index.scss')
    ], { read: false });

    var injectOptions = {
        transform: function(filePath) {
            filePath = filePath.replace(conf.paths.src + '/app/', '');
            return '@import "' + filePath + '";';
        },
        starttag: '// injector',
        endtag: '// endinjector',
        addRootSlash: false
    };


    return gulp.src([
            path.join(conf.paths.src, '/app/index.scss')
        ])
        .pipe($.inject(injectFiles, injectOptions))
        .pipe(wiredep(_.extend({}, conf.wiredep)))
        .pipe($.sourcemaps.init())
        .pipe($.sass(sassOptions)).on('error', conf.errorHandler('Sass'))
        .pipe($.sourcemaps.write())
        .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('dev:scripts', function() {
    return gulp.src([
            path.join(conf.paths.src, '/app/index.module.js'),
            path.join(conf.paths.src, '/app/**/*.js'),
            path.join('!' + conf.paths.src, '/app/**/*.spec.js')
        ])
        .pipe($.eslint())
        .pipe($.eslint.format())
        .pipe($.sourcemaps.init())
        .pipe($.concat('app.js'))
        .pipe($.ngAnnotate())
        .pipe($.uglify({ preserveComments: $.uglifySaveLicense })).on('error', conf.errorHandler('Uglify'))
        .pipe($.sourcemaps.write('maps'))
        .pipe(gulp.dest(conf.paths.dist));
});

gulp.task('dev', ['dev:styles', 'dev:scripts', 'dev:partials'], function() {

    gulp.watch([
        path.join(conf.paths.src, '/app/**/*.css'),
        path.join(conf.paths.src, '/app/**/*.scss')
    ], function() {
        gulp.start('dev:styles');
    });

    gulp.watch(path.join(conf.paths.src, '/app/**/*.js'), function() {
        gulp.start('dev:scripts');
    });

    gulp.watch(path.join(conf.paths.src, '/app/**/*.html'), function() {
        gulp.start('dev:partials');
    });
});