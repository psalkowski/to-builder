<?php

namespace TO\Builder;

use Illuminate\Support\ServiceProvider;
use TO\Builder\Contract\WidgetContract;

class BuilderServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        $this->routes();
    }

    public function routes()
    {
        if (!$this->app->routesAreCached()) {

            $this->app['router']->group(['namespace' => 'TO\Builder\Http\Controllers'], function(){
                require __DIR__.'/Http/routes.php';
            });
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Publish config
        $config = realpath(__DIR__ . '/../config/config.php');

        $this->mergeConfigFrom($config, 'builder');

        $this->publishes([
            $config => config_path('builder.php'),
        ], 'config');

        $this->widgets();
    }

    public function widgets()
    {
        $this->app->singleton('builder.widgets', function ($app) {
            $config = $app['config']->get('builder');
            $widgets = array_get($config, 'widgets');
            $widgetContainer = [];

            foreach ($widgets as $widget) {
                if (class_exists($widget)) {
                    $class = new $widget;

                    if ($class instanceof WidgetContract) {
                        $widgetContainer[] = $class;
                    } else {
                        throw new \Exception("Invalid class %s ", $widget);
                    }
                }
            }

            return $widgetContainer;
        });
    }


    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['builder.widgets'];
    }

}
