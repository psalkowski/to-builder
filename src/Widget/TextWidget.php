<?php

namespace TO\Builder\Widget;

use TO\Builder\Contract\WidgetContract;

class TextWidget implements WidgetContract {

    public function getName()
    {
        return 'Text Widget';
    }

    public function getIcon()
    {
        return null;
    }

    public function getTemplate()
    {
        return 'app/compomnents/text/modal.html';
    }

    public function getController()
    {
        return 'TextController';
    }

    public function render()
    {
        return view('widget.text.view');
    }

    public function getRoute()
    {
        return [
            'main' => route('qeeqweqwe', [], true),
            'items' => ''
        ];
    }
}