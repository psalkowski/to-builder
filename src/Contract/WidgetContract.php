<?php

namespace TO\Builder\Contract;


interface WidgetContract
{
    public function getName();
    public function getIcon();

    public function getTemplate();
    public function getController();

    public function render();
    public function getRoute();

}